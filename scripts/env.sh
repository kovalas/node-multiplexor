#!/bin/bash

log () {
    echo "`date` $1"
}

OUT_DIR=../out
IMAGE_PROXY_ZIP=imgProxy.zip
OUT_ZIP_PATH=$OUT_DIR/$IMAGE_PROXY_ZIP

IMAGE_PROXY_DIR=image-proxy
EXECUTABLE_NAME=app.js
EXECUTABLE_PATH=$IMAGE_PROXY_DIR/$EXECUTABLE_NAME
