#!/bin/bash

#change working directory to script's home
cd ${0%/*}

source env.sh

DEFAULT_PORT=8099
DEFAULT_SSL_DIR=~/ssl
PROCESS_ID=imageProxy

if [ -d $IMAGE_PROXY_DIR ]; then
    log "Cleaning $IMAGE_API_DIR"
    rm -rf $IMAGE_PROXY_DIR
fi

log "Unzipping to $IMAGE_PROXY_DIR"
unzip -q $IMAGE_PROXY_ZIP -d $IMAGE_PROXY_DIR

is_server_running=`forever list | grep $PROCESS_ID | wc -l`

if [ -z $LW_IMAGE_PROXY_PORT ]; then
    log "LW_IMAGE_PROXY_PORT is unset setting it to $DEFAULT_PORT"
    LW_IMAGE_PROXY_PORT=$DEFAULT_PORT
fi

if [ -z $LW_SSL_DIR ]; then
    log "LW_SSL_DIR is unset setting it to $DEFAULT_SSL_DIR"
    LW_SSL_DIR=$DEFAULT_SSL_DIR
fi

export LW_IMAGE_PROXY_PORT
export LW_SSL_DIR

if [ $is_server_running -eq 1 ]
    then
        log "Image proxy server seems to be running. Restarting $EXECUTABLE_PATH"
        forever restart $PROCESS_ID
    else
        log "No running image proxy found. Starting $EXECUTABLE_PATH"
        cd $IMAGE_PROXY_DIR
        forever --uid "$PROCESS_ID" start -a $EXECUTABLE_NAME
fi

