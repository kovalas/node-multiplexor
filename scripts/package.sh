#!/bin/bash

source env.sh

if [ ! -d $OUT_DIR ]; then
    log "No output directory found. Creating $OUT_DIR"
    mkdir $OUT_DIR
fi

log "Cleaning up $OUT_DIR"
rm -rf $OUT_DIR/*

log "Creating package at $OUT_ZIP_PATH"
zip -q -r $OUT_ZIP_PATH ../

log "Copying scripts to $OUT_DIR"
cp ./env.sh ./run.sh $OUT_DIR