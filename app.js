'use strict';

var https = require('https'),
    fs = require('fs'),
    handlerModule = require('./handler.js'),
    port = process.env.LW_IMAGE_PROXY_PORT,
    sslDir = process.env.LW_SSL_DIR;

var sslConfig = {
    key: fs.readFileSync(sslDir + '/godaddy/localway.key'),
    cert: fs.readFileSync(sslDir + '/godaddy/localway.crt'),
    ca: [
        fs.readFileSync(sslDir + '/godaddy/gd1.crt'),
        fs.readFileSync(sslDir + '/godaddy/gd2.crt'),
        fs.readFileSync(sslDir + '/godaddy/gd3.crt'),
        fs.readFileSync(sslDir + '/localway/ca.crt')
    ],
    requestCert: true,
    rejectUnauthorized: true
};

function uploadServersList() {
    var servers;
    var serversList = fs.readFileSync('servers.json');
    serversList = JSON.parse(serversList);

    if (!(serversList instanceof Array) || !serversList.length) {
        console.error('Cannot start with empty servers list from servers.json');
        process.exit(1);
    }

    servers = serversList.map(function (server) {
        return new handlerModule.apiServer(server.host, server.port, sslConfig);
    });

    return servers;
}

if (!sslDir) {
    console.error('LW_SSL_DIR is not set. Exiting...');
    process.exit(1);
}

if (!port) {
    console.error('LW_IMAGE_API_PORT is not set. Exiting...');
    process.exit(1);
}

var handler = handlerModule.handler(uploadServersList(), sslConfig);

https.createServer(sslConfig, handler.handle).listen(port, function () {
    console.log('Image API proxy is listening on port ' + port);
});

