'use strict';

var https = require('https'),
    promise = require('promise'),
    _ = require('underscore')._;

function Handler (servers, sslConfig, https) {
    this.servers = servers;
    this.https = https;
    this.sslConfig = sslConfig;
    var self = this;
    this.handle = function (req, res) {
        promise.all(
            self.servers.map(function (server) {
                return server.processRequest(req);
            })
        )
        .done(function () {
            console.log('Success request ' + req.headers.host + req.url);
            res.writeHead(200);
            res.end();
        }, function (result) {
            res.writeHead(424);
            console.error('[Error] Fail request ' + req.headers.host + req.url);
            if (result.pipe) {
                res.pipe(res, {end: true});
            }
            res.end();
        });

    };
}

var ImageApiServer = function (url, port, sslConfig, httpsRewrite) {
    this.url = url;
    this.port = port;
    this.https = httpsRewrite || https;
    this.sslConfig = sslConfig;
};

ImageApiServer.prototype.processRequest = function (incoming) {
    var self = this;
    var p = new promise(function(resolve, reject){
        var options = {
            hostname: self.url,
            port: self.port,
            path: incoming.url,
            method: incoming.method,
            headers: incoming.headers
        };
        options = _.extend(options, self.sslConfig);
        console.log('Processing request to ' + options.hostname + options.path + ': ' + options.port);
        var req = self.https.request(options, function (res) {
            var logMess = 'Server ' + self.url + ': '+self.port + ' respond with code ' + res.statusCode;
            if (res.statusCode !== 200) {
                console.error('[Error] ' + logMess);
                reject(res);
            } else {
                console.log(logMess);
                resolve(res);
            }
        })
        .on('error', function (err){
            console.error('[Error] Server ' + self.url + ': '+self.port + ': ' + err);
            reject(err);
        });
        incoming.pipe(req, {end: true});
    });
    return p;
};

module.exports = {
    apiServer: ImageApiServer,
    handler: function (servers, sslConfig, httpsRewrited) {
        return new Handler(servers, sslConfig, httpsRewrited || https);
    }
};
