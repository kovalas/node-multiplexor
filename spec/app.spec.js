'use strict';

var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    promise = require('promise'),
    handlerModule = require('../handler.js');

var should = chai.should();

chai.use(sinonChai);

describe('Multiplexor', function () {
    var mockServerOK, mockServerFail;

    before(function () {
        mockServerOK = {
            processRequest: function (){
                var p = new promise(function (resolve) {
                    resolve({});
                });
                return p;
            }
        };

        mockServerFail = {
            processRequest: function (){
                var p = new promise(function (resolve, reject) {
                    reject({});
                });
                return p;
            }
        };

    });

    it('should answer 200 on all servers success', function (done) {
        var mockServers = [mockServerOK, mockServerOK, mockServerOK];
        var handler = handlerModule.handler(mockServers);
        handler.handle({headers: {host: null}}, {
            writeHead: sinon.spy(),
            end: function () {
                this.writeHead.should.calledWith(200);
                done();
            }
        });
    });

    it('should answer 424 on any server fail', function (done) {
        var mockServers = [mockServerOK, mockServerFail, mockServerOK];
        var handler = handlerModule.handler(mockServers);
        handler.handle({headers: {host: null}}, {
            writeHead: sinon.spy(),
            end: function () {
                this.writeHead.should.calledWith(424);
                done();
            }
        });
    });

    it('should answer 424 on all servers fail', function (done) {
        var mockServers = [mockServerFail, mockServerFail, mockServerFail];
        var handler = handlerModule.handler(mockServers);
        handler.handle({headers: {host: null}}, {
            writeHead: sinon.spy(),
            end: function () {
                this.writeHead.should.calledWith(424);
                done();
            }
        });
    });
});


describe('Image api server class', function () {
    var mockHttpsSuccess,
        mockHttpsFail,
        mockHttpsError,
        apiServerSuccess,
        apiServerFail,
        apiServerError;

    before(function (){

        mockHttpsSuccess = {
            request: sinon.spy(function (options, callback) {
                callback({
                    statusCode: 200
                });
                return {
                    on: sinon.spy()
                }
            })
        };

        mockHttpsFail = {
            request: function (options, callback) {
                callback({
                    statusCode: 404
                });
                return {
                    on: sinon.spy()
                }
            }
        };

        mockHttpsError = {
            request: function () {
                return {
                    on: function (eventName, callback) {
                        if (eventName == 'error') {
                            callback();
                        }
                    }
                }
            }
        };

        apiServerSuccess = new handlerModule.apiServer("server.net", 5555, {foo: 'bar'}, mockHttpsSuccess);
        apiServerFail = new handlerModule.apiServer("server.net", 5555, {foo: 'bar'}, mockHttpsFail);
        apiServerError = new handlerModule.apiServer("server.net", 5555, {foo: 'bar'}, mockHttpsError);
    });

    it('should pipe request on success', function (done) {
        var mockRequest = {
            pipe: sinon.spy()
        };

        apiServerSuccess.processRequest(mockRequest).done(function (){
            mockRequest.pipe.should.calledOnce;
            done();
        });

    });

    it('should concat input request options with ssl options', function (done) {
        var mockRequest = {
            url: '/some/path',
            method:'PUT',
            headers: 'some headers',
            pipe: sinon.spy()
        };
        var mockHttpsSuccess = {
            request: function (options) {
                options.hostname.should.be.equal('server.net');
                options.port.should.be.equal(5555);
                options.path.should.be.equal('/some/path');
                options.method.should.be.equal('PUT');
                options.headers.should.be.equal('some headers');
                options.foo.should.be.equal('bar');
                done();
            }
        };

        apiServerSuccess = new handlerModule.apiServer("server.net", 5555, {foo: 'bar'}, mockHttpsSuccess);
        apiServerSuccess.processRequest(mockRequest);
    });
});
